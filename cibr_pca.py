import numpy as np
from sklearn.decomposition import PCA
from matplotlib import pyplot as plt


class activation_PCA:
    def fit(self, X_train=0, X_test=0, num_components=1000):
        n_samples, n_features = X_train.shape
        self.num_components = min(n_samples, n_features) if num_components > min(n_samples, n_features) else num_components
        self.model = PCA(self.num_components)
        self.model.fit(X_train)
        # print('explained_variance_ratio=',self.model.explained_variance_ratio_)
        # principle components are the coefficients that transform original data into principal vectors space.
        self.pcs_train = self.model.transform(X_train)
        if type(X_test)!=int:
            self.pcs_test = self.model.transform(X_test)

xb1=np.load("xb1.npy")
xb2=np.load("xb2.npy")


PCA1=activation_PCA(); PCA1.fit(X_train=xb1,num_components=2)
PCA2=activation_PCA(); PCA2.fit(X_train=xb2,num_components=2)


plt.scatter(PCA2.pcs_train[:,0],PCA2.pcs_train[:,1],s=1)
plt.scatter(PCA1.pcs_train[:,0],PCA1.pcs_train[:,1],s=1)

plt.savefig('foo.png')

