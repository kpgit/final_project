#!/bin/bash
#SBATCH --partition=cibr
#SBATCH --job-name=final
#SBATCH --time=2:00
#SBATCH --output=logs/final_%j.out
#SBATCH --mem=10g

singularity exec cibr_kp578_script.sif python cibr_pca.py
