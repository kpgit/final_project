# README #

### What is this repository for? ###

* this repo is for the final project of cibr

# versions:

* 0.0.1: Save data for CNN and human brain locally
* 0.1.0: Construct a docker file for PCA and python running
* 0.2.0: Draft a script implementing PCA on these data, and visualize the first 2 dimensions.
* 0.3.0: Draft a script for PCA running on Slurm
* 0.4.0: add code to rsync data and code to grace to run slurm
* 0.5.0: Run slurm on grace and save plots
* 1.0.0: finalize and documentation
