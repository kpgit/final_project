## - Create a Dockerfile
cd "/Volumes/GoogleDrive/My Drive/INP/CIBR/homework/final project/"
vim Dockerfile

## - Inside Dockerfilels
FROM python:3.7.5-slim

RUN python -m pip install \
    parse \
    realpython-reader \
    numpy \
    sklearn \
    matplotlib


RUN mkdir /opt/DockerTest

ADD . /opt/DockerTest 

WORKDIR /opt/DockerTest

CMD ["bash"]


## - Build image
docker build -t cibr_image .
docker image ls

docker login


docker container run -it --rm --name cibr_script -v "/Volumes/GoogleDrive/My Drive/INP/CIBR/homework/final project":/opt/DockerTest cibr_image bash 


docker tag 355c6be53a33 kailongpeng/finalproject:lastest
docker push kailongpeng/finalproject:lastest
srun --pty -A cibr -p cibr bash
singularity build cibr_kp578_script.sif docker://kailongpeng/finalproject:lastest
singularity exec cibr_kp578_script.sif bash 

# rsync data
rsync  "/Volumes/GoogleDrive/My Drive/INP/CIBR/homework/final project/xb1.npy"  cibr_kp578@grace.hpc.yale.edu:/gpfs/loomis/project/cibr/cibr_kp578/final_project/
rsync  "/Volumes/GoogleDrive/My Drive/INP/CIBR/homework/final project/xb2.npy"  cibr_kp578@grace.hpc.yale.edu:/gpfs/loomis/project/cibr/cibr_kp578/final_project/

#final run
ssh cibr_kp578@grace.hpc.yale.edu
cd /gpfs/loomis/project/cibr/cibr_kp578/final_project
sbatch cibr_slurm.sh

